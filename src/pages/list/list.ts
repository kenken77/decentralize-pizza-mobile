import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { ItemDetailsPage } from '../item-details/item-details';

import {PizzaService} from '../../providers/pizza-service';
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
  providers: [PizzaService]
})
export class ListPage {
  public pizzas: any;
  selectedItem: any;
  pizza: any;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public pizzaService: PizzaService) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');

    //this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    //'american-football', 'boat', 'bluetooth', 'build'];

    this.items = [];
    /*
    for(let i = 1; i < 11; i++) {
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: 'pizza'
      });
    }*/
    console.log("loading pizzas...");
    this.loadPizzas(this.items);
  }

  loadPizzas(items){
    
    this.pizzaService.load()
    .then(data => {
      data.forEach(contract => {
          this.pizzaService.getPizza(contract).then(data2 =>{
            //console.log(data2);
            this.pizza = data2;
            //console.log(this.pizza.stateInt);
            
            if(this.pizza.stateInt == 2){
              console.log(this.isPizzaInTheItems(contract,items));
              if (!(this.isPizzaInTheItems(contract,items))) {
                  console.log("push?");
                  this.items.push({
                    title: contract,
                    note: this.pizza.pizzaToppings,
                    icon: 'pizza'
                  });
              }
            }
          });
      });
    });
  }

  isPizzaInTheItems(contract, items){
    console.log(contract);
    let isInside: boolean = false;
    if(items.length == 0){
        isInside =  false;
    }

    if(contract == "Latest"){
        isInside = true;
    }

    items.forEach(pizzaItem => {
      if(contract == pizzaItem.title){
        isInside =  true;
      }
    });
    return isInside;
  }

  itemTapped(event, item) {
    this.navCtrl.push(ItemDetailsPage, {
      item: item
    });
  }
}
