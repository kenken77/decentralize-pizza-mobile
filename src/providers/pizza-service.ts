import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the PizzaService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
export class config {
    public static readonly API_END_POINT = "http://192.168.1.48:8000";
}

@Injectable()
export class PizzaService {
  pizzas: any;
  pizza: any;
  constructor(public http: Http) {
    console.log('PizzaService Provider');
  }

  load() {
    if (this.pizzas) {
      // already loaded data
      return Promise.resolve(this.pizzas);
    }

    return new Promise(resolve => {
      this.http.get(config.API_END_POINT + '/contracts/Pizza')
        .map(res => res.json())
        .subscribe(data => {
          //console.log(data);
          this.pizzas = data;
          resolve(this.pizzas);
        });
    });
  }

  getPizza(contract){
    return new Promise((resolve, reject) => {
    
      this.http.get(config.API_END_POINT + '/contracts/Pizza/' + contract + '/state/')
        .map(res => res.json())
        .subscribe(data => {
            //console.log(data);
            this.pizza = data;
            //console.log(this.pizza.stateInt);
            resolve(this.pizza);
        });
    });
  }
  
}
